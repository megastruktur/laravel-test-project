<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    User::create([
      'name' => 'admin',
      'password' => bcrypt('123'),
      'first_name' => 'Mega',
      'last_name' => 'Admin',
      'inner_account_id' => 1,
      'inner_account_add_amount' => 1,
    ]);
  }

}
