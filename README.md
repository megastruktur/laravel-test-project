# Laravel Test app (.env file included)

### Details
1. Added [Passport](https://github.com/laravel/passport) library for oAuth stuff
2. Added [Agent](https://github.com/jenssegers/agent) library for user agent tracking.

### Installation with Docker
1. Make sure you have installed:
- docker (run `docker --version`)
- docker-compose (run `docker-compose --version`)
2. Run `docker-compose up -d` inside the application directory. This will create all necessary docker containers. Useful commands:
```bash

  $ docker-compose up -d         # start containers in background
  $ docker-compose kill          # stop containers
  $ docker-compose up -d --build # force rebuild of Dockerfiles
  $ docker-compose rm            # remove stopped containers
  $ docker ps                    # see list of running containers
  $ docker exec -ti [NAME] bash
  
```
3. Install application:

```

  $ composer install
  $ php artisan migrate
  $ php artisan db:seed

```
4. Run `php artisan passport:install` to install passport library stuff.
5. Run `php artisan vendor:publish` to publish views (I think it can be optional though)
6. Go to `http://localhost:8090` - you should see Laravel welcome page.

### Endpoints and usage

1. POST: `/api/login`

Params:
- grant_type: password
- name: admin
- password: 123

Description: Will return 401 on invalid credentials OR TOKEN on success. On successful login the system will store logged user details into `access_log` table.

2. GET: `/api/details`

Headers:
- Authorization: Bearer TOKEN

Description: Will return 401 on invalid TOKEN OR User JSON object on success.
