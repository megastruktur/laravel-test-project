<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccessLog extends Model {

//  public $user_id;
//  public $ip;
//  public $device;
//  public $os;
//  public $os_version;
  
  
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'user_id',
    'ip',
    'device',
    'os',
    'os_version',
  ];
  
}
