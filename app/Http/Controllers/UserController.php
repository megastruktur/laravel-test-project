<?php

/**
 * Controller for User actions.
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AccessLogController;

class UserController extends Controller {

  public $successStatus = 200;

  /**
   * Login api
   *
   * @return \Illuminate\Http\Response
   */
  public function login() {
    
    if (Auth::attempt(['name' => request('name'), 'password' => request('password')])) {
      $user = Auth::user();
      $success['token'] = $user->createToken('MyApp')->accessToken;
      
      // Log successful login attempt.
      AccessLogController::log();
      
      return response()->json(['success' => $success], $this->successStatus);
    } else {
      return response()->json(['error' => 'Unauthorised'], 401);
    }
  }

  /**
   * Some details about user.
   *
   * @return \Illuminate\Http\Response
   */
  public function details() {
    $user = Auth::user();
    return response()->json(['success' => $user], $this->successStatus);
  }

}
