<?php

namespace App\Http\Controllers;

use App\AccessLog;
use Illuminate\Support\Facades\Auth;
use Jenssegers\Agent\Agent as Agent;

class AccessLogController extends Controller {
  
  /**
   * Store access log into the database.
   */
  public static function log() {
    
    $AL = new AccessLog();
    $User = Auth::user();
    
    if ($User) {
      $Agent = new Agent();
      
      // Set user.
      $AL->user_id = $User->id;

      // Set ip.
      $AL->ip = \Request::ip();

      // Set the Device.
      if ($Agent->isDesktop()) {
        $device = 'PC';
      } else {
        $device = $Agent->device();
      }
      $AL->device = $device;

      // Set the platform and version.
      $AL->os = $Agent->platform();
      $AL->os_version = $Agent->version($AL->os);
      
      // Save the data.
      $AL->save();
    }
  }
}