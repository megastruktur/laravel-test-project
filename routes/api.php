<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


/**
 * Login.
 * @param string name
 * @param string password
 * @return string JSON {"success":{token:"blabla"}} or 401 Unauthorized.
 */
Route::post('login', [
  'uses' => 'UserController@login',
  'as' => 'login',
]);

/**
 * Auth endpoints (endpoints which require Authorization: Bearer TOKEN header).
 */
Route::group(['middleware' => 'auth:api'], function(){
  
  // Details.
  Route::get('details', 'UserController@details');
});